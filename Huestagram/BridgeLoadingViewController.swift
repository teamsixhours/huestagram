//
//  BridgeLoadingViewController.swift
//  Huestagram
//
//  Created by 성준영 on 2016. 7. 7..
//  Copyright © 2016년 JUNYOUNG. All rights reserved.
//

import UIKit
import SwiftyHue
import Gloss
import SwiftyButton



class BridgeLoadingViewController : UIViewController{
    @IBOutlet weak var activityIndicatior: UIActivityIndicatorView!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    var bridge : HueBridge!
    var bridgeAuthenticator: BridgeAuthenticator!
    var bridgeAccessConfig: BridgeAccessConfig!
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.Portrait
    }
    
    
    let bridgeFinder = BridgeFinder()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        startButton.hidden = true;
        image.hidden = true;
        
        
        bridgeFinder.delegate = self;
        bridgeFinder.start()
      
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    
}

extension BridgeLoadingViewController: BridgeFinderDelegate {
    
    func bridgeFinder(finder: BridgeFinder, didFinishWithResult bridges: [HueBridge]) {
        print("find bridge!")
        print(bridges[0])
        Bridge.bridge = bridges[0]
        
        self.bridge = bridges[0]
        
        print(bridges.description)
        bridgeAuthenticator = BridgeAuthenticator(bridge: bridge, uniqueIdentifier: "swiftyhue#\(UIDevice.currentDevice().name)")
        bridgeAuthenticator.delegate = self;
        bridgeAuthenticator.start()
        
    }
}

extension BridgeLoadingViewController: BridgeAuthenticatorDelegate {
    
    func bridgeAuthenticator(authenticator: BridgeAuthenticator, didFinishAuthentication username: String) {
        print("finish Authentication")
        print(username)
        Bridge.username = username
        self.bridgeAccessConfig = BridgeAccessConfig(bridgeId: "BridgeId", ipAddress: bridge.ip, username: username)
        self.activityIndicatior.stopAnimating()
        
        label.hidden = true;
        
        UIView.transitionWithView(view, duration: 1.0, options: .TransitionCrossDissolve, animations: {() -> Void in
            self.image.hidden = false
            }, completion: { _ in })
        UIView.transitionWithView(view, duration: 1.0, options: .TransitionCrossDissolve, animations: {() -> Void in
            self.startButton.hidden = false
            }, completion: { _ in })
        
        
        startButton.layer.borderColor =  UIColor.whiteColor().CGColor
        startButton.layer.borderWidth = 1
        startButton.layer.cornerRadius = 5
        

    }
    
    func bridgeAuthenticator(authenticator: BridgeAuthenticator, didFailWithError error: NSError) {
        print(error)
    }
    
    // you should now ask the user to press the link button
    func bridgeAuthenticatorRequiresLinkButtonPress(authenticator: BridgeAuthenticator) {
        self.label.text = "링크 버튼을 클릭해주세요"
        print("click link button")
    }
    
    // user did not press the link button in time, you restart the process and try again
    func bridgeAuthenticatorDidTimeout(authenticator: BridgeAuthenticator) {
        print("time out")
    }
}